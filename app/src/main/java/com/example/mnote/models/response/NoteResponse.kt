package com.example.mnote.models.response

import com.example.mnote.models.entity.NoteEntity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NoteResponse(
    @SerializedName("success")
    @Expose
    var success: Int?,
    @SerializedName("msg")
    @Expose
    var msg: String?,
    @SerializedName("notes")
    @Expose
    var notes: List<NoteEntity>?
)