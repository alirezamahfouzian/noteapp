package com.example.mnote.models.response

data class UserResponseModel (
    var name: String,
    var email: String
)