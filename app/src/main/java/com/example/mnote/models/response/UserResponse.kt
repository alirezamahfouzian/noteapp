package com.example.mnote.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserResponse(

    @Expose
    var success: Int?,
    @SerializedName("user_id")
    @Expose
    var userId: String?,
    @SerializedName("user")
    @Expose
    var userModel: UserResponseModel?
)