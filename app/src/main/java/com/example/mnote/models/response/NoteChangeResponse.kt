package com.example.mnote.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NoteChangeResponse(
    @Expose
    var success: Int,
    @SerializedName("note_id")
    @Expose
    val noteId: Int,
    @SerializedName("msg")
    @Expose
    var message: String
)