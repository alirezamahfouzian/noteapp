package com.example.mnote.models.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "operation")
data class OperationEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "pk")
    var pk: Int?,
    @ColumnInfo(name = "note_id")
    var noteId: Int? = -1,
    @ColumnInfo(name = "title")
    val title: String?,
    @ColumnInfo(name = "note")
    val note: String?,
    @ColumnInfo(name = "modified_time")
    var modifiedTime: String?,
    /**
     * 1 = insert
     * 2 = edit
     * 3 = delete
     */
    @ColumnInfo(name = "operation_type")
    var operationType: Int
)