package com.example.mnote.models.entry

import com.google.gson.annotations.Expose

data class RegisterEntry(
    @Expose
    var name: String,
    @Expose
    var email: String,
    @Expose
    var password: String
)