package com.example.mnote.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NoteModel(

    @SerializedName("note_id")
    @Expose
    var noteId: String?,
    @SerializedName("user_id")
    @Expose
    var userId: String?,
    @Expose
    var title: String?,
    @Expose
    var note: String?,
    @Expose
    @SerializedName("modified_time")
    var modifiedTime: String?
)