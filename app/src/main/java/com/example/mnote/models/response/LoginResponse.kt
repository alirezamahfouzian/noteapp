package com.example.mnote.models.response

import com.google.gson.annotations.Expose

data class LoginResponse(
    @Expose
    var success: Int?,
    @Expose
    var token: String?,
    @Expose
    var message: String?
)