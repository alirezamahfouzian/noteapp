package com.example.mnote.models.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "token"
)
data class TokenEntity(

    @ColumnInfo(name = "user_id")
    @PrimaryKey(autoGenerate = false)
    var userId: Int = -1,
    @ColumnInfo(name = "token")
    var token: String? = null

)