package com.example.mnote.models.response

import com.google.gson.annotations.Expose

data class RegisterResponse(
    @Expose
    var success: Int?,
    @Expose
    var status: Int?,
    @Expose
    var message: String?,
    @Expose
    var token: String?
)