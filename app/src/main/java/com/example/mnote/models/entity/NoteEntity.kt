package com.example.mnote.models.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "note")
data class NoteEntity(

    @Expose
    @SerializedName("note_id")
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "note_id")
    var noteId: Int?,
    @Expose
    @ColumnInfo(name = "title")
    val title: String?,
    @Expose
    @ColumnInfo(name = "note")
    val note: String?,
    @Expose
    @SerializedName("modified_time")
    @ColumnInfo(name = "modified_time")
    var modifiedTime: String?


) {
    // for diff util comparison
    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass)
            return false

        other as NoteEntity

        if (noteId != other.noteId)
            return false
        if (title != other.title)
            return false
        if (note != other.note)
            return false
        if (modifiedTime != other.modifiedTime)
            return false

        return true
    }
}