package com.example.mnote.models.entry

import com.google.gson.annotations.Expose

data class LoginEntry(
    @Expose
    var email: String,
    @Expose
    var password: String
)