package com.example.mnote.models.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity(

    @ColumnInfo(name = "user_id")
    @PrimaryKey(autoGenerate = false)
    var user_id: Int = -1,
    @ColumnInfo(name = "name")
    var name: String? = null,
    @ColumnInfo(name = "family")
    var family: String? = null,
    @ColumnInfo(name = "email")
    var email: String? = null

)