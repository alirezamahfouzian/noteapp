package com.example.mnote.models.maper

import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.models.entity.OperationEntity

class NoteMapper {
    companion object {
        fun toEntity(operationEntity: OperationEntity): NoteEntity {
            return NoteEntity(
                operationEntity.noteId,
                operationEntity.title,
                operationEntity.note,
                operationEntity.modifiedTime
            )
        }

        fun toOperation(noteEntity: NoteEntity, operationType: Int): OperationEntity {
            return OperationEntity(
                null,
                noteEntity.noteId,
                noteEntity.title,
                noteEntity.note,
                noteEntity.modifiedTime,
                operationType
            )
        }
    }
}