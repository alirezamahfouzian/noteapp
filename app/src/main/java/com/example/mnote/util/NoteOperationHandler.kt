package com.example.mnote.util

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.models.entity.OperationEntity
import com.example.mnote.models.maper.NoteMapper
import com.example.mnote.models.response.NoteChangeResponse
import com.example.mnote.models.response.NoteResponse
import com.example.mnote.network.api.NoteApi
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.persistence.NoteDao
import com.example.mnote.persistence.OperationDao
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject

/**
 * operation Types:
 * 1 = insert
 * 2 = edit
 * 3 = delete
 */
class NoteOperationHandler @Inject constructor(
    private val operationDao: OperationDao,
    private val noteDao: NoteDao,
    private val noteApi: NoteApi,
    private val connection: ConnectionLiveData
) {
    private val TAG = "NoteOperationHandler"
    private val checkSync: MediatorLiveData<Boolean> = MediatorLiveData()
    private val noteResponse: MediatorLiveData<ApiResponse<NoteResponse>> = MediatorLiveData()
    private lateinit var job: CompletableJob
    private lateinit var coroutineScope: CoroutineScope
    private var lastOperationPk = -1


    init {
        initJob()
    }

    private fun initJob(): Job {
        job = Job()
        coroutineScope = CoroutineScope(Dispatchers.IO + job)
        return job
    }

    fun syncServerOperations(): LiveData<Boolean> {
        GlobalScope.launch(IO) {
            val operationList = operationDao.getOperations()
            if (operationList.isNotEmpty()) {
                operationList.forEach { entity ->
                    Log.d(TAG, "syncServerOperations for: ${entity.pk}")

                    if (entity == operationList.last())
                        lastOperationPk = entity.pk!!
                    withContext(Main) {
                        if (connection.isConnected) {
                            when (entity.operationType) {
                                1 -> addNoteServer(entity)
                                2 -> editNoteServer(entity)
                                3 -> deleteNoteServer(entity)
                            }
                        }
                    }
                }
            } else {
                Log.d(TAG, "withContext: ")
                checkSync.postValue(true)
            }
        }
        return checkSync
    }

    fun addNote(noteEntity: NoteEntity) {
        coroutineScope.launch {
            val noteId = noteDao.addNote(noteEntity)
            noteEntity.noteId = noteId.toInt()
            val operation = NoteMapper.toOperation(noteEntity, 1)
            val operationPk = operationDao.addOperation(operation)
            operation.pk = operationPk.toInt()
            Log.d(TAG, "addNote: $operation")
            withContext(Main) {
                addNoteServer(operation)
            }
        }
    }

    private fun addNoteServer(operation: OperationEntity) {
        handleNetwork(
            noteApi.addNote(
                operation.title!!,
                operation.note!!,
                operation.modifiedTime!!
            ),
            operation
        )
    }

    fun editNote(noteEntity: NoteEntity) {
        Log.d(TAG, "editNote: $noteEntity")
        coroutineScope.launch {
            noteDao.updateNote(noteEntity)
            val operation = NoteMapper.toOperation(noteEntity, 2)
            val operationPk = operationDao.addOperation(operation)
            operation.pk = operationPk.toInt()
            withContext(Main) {
                editNoteServer(operation)
            }
        }
    }

    private fun editNoteServer(operation: OperationEntity) {
        Log.d(TAG, "editNoteServer: $operation")
        handleNetwork(
            noteApi.editNote(
                operation.noteId!!,
                operation.title!!,
                operation.note!!,
                operation.modifiedTime!!
            ),
            operation
        )
    }

    fun deleteNote(noteEntity: NoteEntity) {
        coroutineScope.launch {
            noteDao.deleteNote(noteEntity)
            val operation = NoteMapper.toOperation(noteEntity, 3)
            val operationPk = operationDao.addOperation(operation)
            operation.pk = operationPk.toInt()
            withContext(Main) {
                deleteNoteServer(operation)
            }
        }
    }

    private fun deleteNoteServer(operation: OperationEntity) {
        handleNetwork(
            noteApi.deleteNote(
                operation.noteId!!
            ),
            operation
        )
    }

    private fun handleNetwork(
        response: LiveData<ApiResponse<NoteChangeResponse>>,
        operationEntity: OperationEntity
    ) {
        Log.d(TAG, "handleNetwork: ${connection.isConnected}")
        if (!connection.isConnected)
            return

        response.observeForever(Observer {
            job = Job()
            GlobalScope.launch(IO + job) {
                when (it) {
                    is ApiSuccessResponse -> {
                        Log.d(TAG, "ApiSuccessResponse: ${it.body}")
                        if (it.body.success == 1) {
                            if (operationEntity.operationType == 1) {
                                noteDao.updateNoteId(
                                    operationEntity.noteId!!,
                                    it.body.noteId
                                )
                            }
                            operationDao.deleteOperation(operationEntity)
                        } else if (it.body.message == "No Note Found") {
                            operationDao.deleteOperation(operationEntity)
                        }
                        job.complete()
                    }
                    is ApiEmptyResponse -> {
                        Log.d(TAG, "ApiEmptyResponse: $it")
                        job.complete()
                    }
                    is ApiErrorResponse -> {
                        Log.d(TAG, "ApiErrorResponse: ${it.errorMessage}")
                        job.complete()
                    }
                }
            }
            job.invokeOnCompletion {
                postValueSync(operationEntity.pk!!)
            }
        })
    }

    private fun postValueSync(pk: Int) {
        if (lastOperationPk == pk) {
            Log.d(TAG, "syncServerOperations postValueSync $pk  $lastOperationPk")
            lastOperationPk = -1
            checkSync.postValue(true)
        }
    }
}