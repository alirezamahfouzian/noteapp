package com.example.mnote.util

class ErrorHandler {
    companion object {
        const val NO_INTERNET = "No internet Connection!!"
        const val ERROR_UNKNOWN = "unKnown Error"
        const val CANT_SYNC = "Your Notes are not Synced with Server"
    }
}