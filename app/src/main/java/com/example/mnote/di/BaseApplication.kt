package com.example.mnote.di

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.FragmentKeys
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class BaseApplication : Application() {

    @Inject
    lateinit var mPref: SharedPreferences

    @Inject
    lateinit var connectionLiveData: ConnectionLiveData

    var isLight = true

    override fun onCreate() {
        super.onCreate()
        changeThemeStart()
        checkNetwork()
    }

    private fun changeThemeStart() {
        isLight = mPref.getBoolean(FragmentKeys.THEME_PREFS_KEY, false)
        if (isLight) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }

    private fun checkNetwork() {

    }
}