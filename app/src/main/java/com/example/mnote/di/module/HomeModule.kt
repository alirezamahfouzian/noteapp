package com.example.mnote.di.module

import com.example.mnote.persistence.AppDataBase
import com.example.mnote.persistence.NoteDao
import com.example.mnote.persistence.OperationDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class HomeModule {

    @Provides
    @Singleton
    fun provideNoteDao(dataBase: AppDataBase): NoteDao {
        return dataBase.getNoteDao()
    }

    @Provides
    @Singleton
    fun provideOperation(dataBase: AppDataBase): OperationDao {
        return dataBase.getOperationDao()
    }

}