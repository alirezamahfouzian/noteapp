package com.example.mnote.di.module

import com.example.mnote.network.api.AuthApi
import com.example.mnote.persistence.AppDataBase
import com.example.mnote.persistence.TokenDao
import com.example.mnote.persistence.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class AuthModule {

    @Provides
    fun providesAuthApi(retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }

    @Provides
    @Singleton
    fun provideTokenDao(dataBase: AppDataBase): TokenDao {
        return dataBase.getTokenDao()
    }

    @Provides
    @Singleton
    fun provideUserDao(dataBase: AppDataBase): UserDao {
        return dataBase.getUserDao()
    }
}