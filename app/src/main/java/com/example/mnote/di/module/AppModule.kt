package com.example.mnote.di.module


import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.room.Room
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.persistence.AppDataBase
import com.example.mnote.persistence.AppDataBase.Companion.DATABASE_NAME
import com.example.mnote.ui.Hilt_MainActivity
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.ActivityHelper
import com.example.mnote.ui.helper.FragmentKeys.Companion.SHARED_PREF
import com.example.mnote.util.Constants
import com.example.mnote.util.LiveDataCallAdapterFactory
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@InstallIn(ApplicationComponent::class)
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideRoom(application: Application): AppDataBase {
        return Room.databaseBuilder(application, AppDataBase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
//            .excludeFieldsWithoutExposeAnnotation()
        return gsonBuilder.create()
    }

    @Provides
    @Singleton //providing tokenInterceptor like this
    fun provideOkhttp(tokenInterceptor: TokenInterceptor): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okhttpBuilder =
            OkHttpClient().newBuilder().addInterceptor(interceptor).addInterceptor(tokenInterceptor)
        return okhttpBuilder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun providesHelper(): ActivityHelper.Helper {
        return ActivityHelper.Helper()
    }

    @Singleton
    @Provides
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences(
            SHARED_PREF,
            Context.MODE_PRIVATE
        )
    }

    @Singleton
    @Provides
    fun providesSharedPreferencesEditor(pref: SharedPreferences): SharedPreferences.Editor {
        return pref.edit()
    }

    @Singleton
    @Provides
    fun providesBundle(): Bundle {
        return Bundle()
    }
}