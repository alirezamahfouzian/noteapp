package com.example.mnote.di.module

import com.example.mnote.network.api.AuthApi
import com.example.mnote.network.api.NoteApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import retrofit2.Retrofit

@InstallIn(ActivityComponent::class)
@Module
class ActivityModule {

    @Provides
    fun providesNoteApi(retrofit: Retrofit): NoteApi {
        return retrofit.create(NoteApi::class.java)
    }

}