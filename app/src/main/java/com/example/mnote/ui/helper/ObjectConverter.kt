package com.example.mnote.ui.helper

class ObjectConverter {

    companion object {
        fun getName(fullName: String): List<String> {
            val spacePosition = fullName.indexOf(' ')
            val name = fullName.substring(0, spacePosition)
            val family = fullName.substring(spacePosition + 1, fullName.length)
            return arrayListOf(name, family)
        }
    }

}