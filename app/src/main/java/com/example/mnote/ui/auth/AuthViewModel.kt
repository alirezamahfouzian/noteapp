package com.example.mnote.ui.auth

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.mnote.models.entity.TokenEntity
import com.example.mnote.models.entity.UserEntity
import com.example.mnote.models.entry.LoginEntry
import com.example.mnote.models.entry.RegisterEntry
import com.example.mnote.models.response.LoginResponse
import com.example.mnote.models.response.RegisterResponse
import com.example.mnote.models.response.UserResponse
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.network.helper.Resource
import com.example.mnote.repository.auth.AuthRepository
import com.example.mnote.ui.helper.ObjectConverter
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthViewModel @ViewModelInject constructor(
    private val authRepository: AuthRepository,
    private var tokenInterceptor: TokenInterceptor,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel(), LifecycleObserver {

    private val TAG = "AuthViewModel"
    var userId: Int = -1
    private val _loginLiveData: MutableLiveData<Resource<LoginResponse>> = MutableLiveData()
    private val _registerLiveData: MediatorLiveData<Resource<RegisterResponse>> =
        MediatorLiveData()
    private val _userLiveData: MutableLiveData<Resource<UserResponse>> = MutableLiveData()

    val userLiveData: LiveData<Resource<UserResponse>> get() = _userLiveData
    val loginLiveData: LiveData<Resource<LoginResponse>> get() = _loginLiveData
    val registerLiveData: LiveData<Resource<RegisterResponse>> get() = _registerLiveData

    fun login(loginEntry: LoginEntry) {
        _loginLiveData.postValue(Resource.Loading(null))
        authRepository.login(loginEntry).enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (!response.body()?.token.isNullOrEmpty() && response.body()?.success == 1) {
                    Log.d(TAG, "login onResponse: ${response.body()}")
                    _loginLiveData.value = Resource.Success(response.body()!!)
                } else if (response.body()?.success == 0) {
                    Log.d(TAG, "login onResponse: ${response.body()}")
                    _loginLiveData.value = Resource.Error(response.body()!!.message!!, null)

                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d(TAG, "login onFailure: ${t.message}")
                _loginLiveData.value = Resource.Error(t.message.toString(), null)
            }
        })
    }

    fun register(registerEntry: RegisterEntry) {
        _registerLiveData.postValue(Resource.Loading(null))

        authRepository.register(registerEntry).enqueue(object : Callback<RegisterResponse> {
            override fun onResponse(
                call: Call<RegisterResponse>,
                response: Response<RegisterResponse>
            ) {
                if (!response.body()?.token.isNullOrEmpty() && response.body()?.success == 1) {
                    Log.d(TAG, "register onResponse: ${response.body()}")
                    _registerLiveData.value = Resource.Success(response.body()!!)
                } else if (response.body()?.success == 0) {
                    Log.d(TAG, "login onResponse: ${response.body()}")
                    _registerLiveData.value = Resource.Error(response.body()!!.message!!, null)

                }
            }

            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                Log.d(TAG, "register onFailure: ${t.message}")
                _registerLiveData.value = Resource.Error(t.message.toString(), null)
            }
        })
    }

    suspend fun addUserReplace(userEntity: UserEntity): Long {
        return authRepository.addUserReplace(userEntity)
    }

    fun getUser() {
        _userLiveData.value = Resource.Loading(null)
        authRepository.getUser().enqueue(object : Callback<UserResponse> {
            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.body()?.success == 1) {
                    Log.d(TAG, "getUser onResponse: ${response.body()}")
                    val data: UserResponse? = response.body()
                    val userId: Int? = data!!.userId.toString().toInt()
                    val name = ObjectConverter.getName(data.userModel!!.name)
                    GlobalScope.launch(IO) {
                        UserEntity(
                            userId!!, name[0], name[1], data.userModel!!.email
                        ).let { userEntity ->
                            addUserReplace(userEntity)
                        }
                        TokenEntity(userId, tokenInterceptor.token).let { token ->
                            addToken(token)
                        }
                        withContext(Main) {
                            _userLiveData.value = Resource.Success(response.body()!!)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                _userLiveData.value = Resource.Error(t.message.toString(), null)
            }
        })
    }

    suspend fun addToken(tokenEntity: TokenEntity): Long {
        return authRepository.addToken(tokenEntity)
    }

    fun getToken(): LiveData<TokenEntity> {
        return authRepository.getToken()
    }

//    val token: LiveData<TokenEntity> get() = authRepository.getToken()

    fun deleteToken(userId: Int): Int {
        var row: Int = -1
        GlobalScope.launch(IO) {
            row = authRepository.deleteToken(userId)
        }
        return row
    }
}