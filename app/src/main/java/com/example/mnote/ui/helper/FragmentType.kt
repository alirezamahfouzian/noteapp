package com.e.fiting.ui.start_activity.ui.helpers

enum class FragmentType {
    LOGIN_FRAGMENT, SIGNUP_FRAGMENT
}