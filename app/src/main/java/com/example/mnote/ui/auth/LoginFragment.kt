package com.example.mnote.ui.auth

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.e.fiting.ui.start_activity.ui.helpers.FragmentType
import com.example.mnote.R
import com.example.mnote.di.BaseApplication
import com.example.mnote.models.entry.LoginEntry
import com.example.mnote.network.api.AuthApi
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.network.helper.Resource
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.AddEditTextFocusEffect
import com.example.mnote.ui.helper.FragmentKeys.Companion.NUMBER_KEY
import com.example.mnote.ui.helper.FragmentKeys.Companion.PASSWORD_KEY
import com.example.mnote.ui.helper.FragmentListener
import com.example.mnote.util.ErrorHandler.Companion.NO_INTERNET
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login),
    View.OnClickListener {

    private val TAG = "LoginFragment"

    @Inject
    lateinit var tokenInterceptor: TokenInterceptor

    @Inject
    lateinit var mPref: SharedPreferences

    @Inject
    lateinit var mAuthApi: AuthApi

    @Inject
    lateinit var mEditor: SharedPreferences.Editor

    @Inject
    lateinit var connectionLiveData: ConnectionLiveData

    val authViewModel: AuthViewModel by viewModels()

    private lateinit var navController: NavController
    private var mSavedInstance: Bundle? = null
    private lateinit var mActivity: MainActivity
    private var token: String? = null
    lateinit var baseApplication: BaseApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as MainActivity
        baseApplication = mActivity.application as BaseApplication

    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        build()
    }

    override fun onStop() {
        super.onStop()
        saveInstance()
    }

    private fun build() {
        buttonSubmitLogin!!.setOnClickListener(this)
        textViewGoToSignUp!!.setOnClickListener(this)
        restoreFragment()
        addFocusEffect()
        getUser()
        login()
    }

    /** gets triggered when {authViewModel.getUser()} get called*/
    private fun getUser() {
        authViewModel.userLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    navController.navigate(R.id.action_loginFragment_to_homeFragment)
                }
                is Resource.Error -> {
                    constraintLogin.visibility = View.VISIBLE
                    mActivity.apply {
                        showProgressBar(false)
                        showSnackBar(it.message.toString())
                    }
                }
            }

        })
    }

    private fun login() {
        authViewModel.loginLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Loading -> {
                    isLoading(true)
                }
                is Resource.Success -> {
                    token = it.data?.token
                    tokenInterceptor.token = token
                    authViewModel.getUser()
                }
                is Resource.Error -> {
                    isLoading(false)
                    mActivity.showSnackBar(it.message.toString())
                }
            }
        })
    }

    private fun isLoading(isLoading: Boolean) {
        if (isLoading) {
            buttonSubmitLogin.isClickable = true
            constraintLogin.visibility = View.GONE
            mActivity.showProgressBar(isLoading)
        } else {
            buttonSubmitLogin.isClickable = false
            constraintLogin.visibility = View.VISIBLE
            mActivity.showProgressBar(isLoading)
        }
    }

    /**
     * calls the [AddEditTextFocusEffect] and sets
     * the text view change effect
     */
    private fun addFocusEffect() {
        AddEditTextFocusEffect(
            null, null, null,
            null, editTextEmail, textViewPhoneNumberHint,
            editTextPassword, textViewPasswordHint!!, buttonSubmitLogin
        )
            .execute(FragmentType.LOGIN_FRAGMENT)
    }

    /**
     * passes a bundle to  and stores it data
     */
    private fun saveInstance() {
        mSavedInstance = Bundle()
        mSavedInstance?.putString(NUMBER_KEY, editTextEmail!!.text.toString())
        mSavedInstance?.putString(PASSWORD_KEY, editTextPassword!!.text.toString())
    }

    /**
     * uses stored data in loginFragment whenever loginFragment starts again
     * after an onBackPressed
     */
    private fun restoreFragment() {
        if (mSavedInstance != null) {
            val number =
                mSavedInstance?.getString(FragmentListener.NUMBER_KEY)
            val password =
                mSavedInstance?.getString(FragmentListener.PASSWORD_KEY)
            if (number != null) {
                editTextEmail!!.setText(number)
            }
            if (password != null) {
                editTextPassword!!.setText(password)
            }
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.buttonSubmitLogin -> {
                buttonSubmitLogin.isClickable = false
                if (connectionLiveData.isConnected) {
                    var loginEntity =
                        LoginEntry(
                            editTextEmail.text.toString(),
                            editTextPassword.text.toString()
                        )
                    authViewModel.login(loginEntity)
                } else
                    mActivity.showSnackBar(NO_INTERNET)
            }
            R.id.textViewGoToSignUp ->
                navController.navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

}