package com.example.mnote.ui.helper

import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.Nullable
import com.e.fiting.ui.start_activity.ui.helpers.FragmentType
import com.example.mnote.R
import java.util.regex.Pattern

class AddEditTextFocusEffect(
    @Nullable var mEditTextName: EditText? = null,
    @Nullable var mTextViewNameHint: TextView? = null,
    @Nullable var mEditTextFamilyName: EditText? = null,
    @Nullable var mTextViewFamilyNameHint: TextView? = null,
    private var mEditTextPhoneNumber: EditText?,
    private var mTextViewPhoneNumberHint: TextView?,
    private var mEditTextPassword: EditText?,
    private var mTextViewPasswordHint: TextView,
    private var mButtonComit: Button?
) {

    private var isName: Boolean = true
    private var isFamilyName: Boolean = true
    private var isNumber: Boolean = false
    private var isPassword: Boolean = false

    private var type: FragmentType? = null

    fun execute(type: FragmentType) {
        this.type = type

        if (type == FragmentType.LOGIN_FRAGMENT) {
            creatEditTextFocusEffectBackPressedLogin()
            return
        } else {
            creatEditTextFocusEffectBackPressedSignup()
        }

    }

    private fun creatEditTextFocusEffectBackPressedLogin() {
        /** if we call back button from sign up we gonna come to this method **/

        if (!mEditTextPhoneNumber?.text.isNullOrEmpty()) {
            mTextViewPhoneNumberHint?.visibility = View.VISIBLE
            checkEditTextInput(
                mEditTextPhoneNumber!!,
                mTextViewPhoneNumberHint!!,
                mEditTextPhoneNumber?.text.toString()
            )
        }

        if (!mEditTextPassword?.text.isNullOrEmpty()) {
            mTextViewPasswordHint.visibility = View.VISIBLE
            checkEditTextInput(
                mEditTextPassword!!,
                mTextViewPasswordHint,
                mEditTextPassword?.text.toString()
            )
        }

        setOnFocusChangeListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, "Email")

        setOnFocusChangeListener(mEditTextPassword!!, mTextViewPasswordHint, "Password")

        setTextChangedListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!)

        setTextChangedListener(mEditTextPassword!!, mTextViewPasswordHint)

    }

    private fun creatEditTextFocusEffectBackPressedSignup() {
        /** if we call back button from sign up we gonna come to this method **/

        if (!mEditTextName?.text.isNullOrEmpty()) {
            mTextViewNameHint?.visibility = View.VISIBLE
            checkEditTextInput(mEditTextName!!, mTextViewNameHint!!, mEditTextName?.text.toString())
        }

        if (!mEditTextFamilyName?.text.isNullOrEmpty()) {
            mTextViewFamilyNameHint?.visibility = View.VISIBLE
            checkEditTextInput(
                mEditTextFamilyName!!,
                mTextViewFamilyNameHint!!,
                mEditTextName?.text.toString()
            )
        }

        if (!mEditTextPhoneNumber?.text.isNullOrEmpty()) {
            mTextViewPhoneNumberHint?.visibility = View.VISIBLE
            checkEditTextInput(
                mEditTextPhoneNumber!!,
                mTextViewPhoneNumberHint!!,
                mEditTextPhoneNumber?.text.toString()
            )
        }

        if (!mEditTextPassword?.text.isNullOrEmpty()) {
            mTextViewPasswordHint.visibility = View.VISIBLE
            checkEditTextInput(
                mEditTextPassword!!,
                mTextViewPasswordHint,
                mEditTextPassword?.text.toString()
            )
        }

        setOnFocusChangeListener(mEditTextName!!, mTextViewNameHint!!, "Name")

        setOnFocusChangeListener(mEditTextFamilyName!!, mTextViewFamilyNameHint!!, "Family")


        setOnFocusChangeListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!, "Email")

        setOnFocusChangeListener(mEditTextPassword!!, mTextViewPasswordHint, "password")

        setTextChangedListener(mEditTextName!!, mTextViewNameHint!!)

        setTextChangedListener(mEditTextFamilyName!!, mTextViewFamilyNameHint!!)

        setTextChangedListener(mEditTextPhoneNumber!!, mTextViewPhoneNumberHint!!)

        setTextChangedListener(mEditTextPassword!!, mTextViewPasswordHint)

    }

    fun addEditTextFocusEffect(
        isEmpty: Boolean, hasFocus: Boolean, editText: EditText,
        textViewHint: TextView, _hint: String
    ) {
        /** makes the effect of editTexts */

        if (!hasFocus && isEmpty) {
            if (textViewHint.id == R.id.textViewFamilyNameHint) {

                editText.hint = _hint
                textViewHint.visibility = View.INVISIBLE

            } else {
                editText.hint = _hint
                textViewHint.visibility = View.GONE
            }
            return
        }

        if (hasFocus && !isEmpty) {
            textViewHint.visibility = View.VISIBLE
            return
        }

        if (!hasFocus && !isEmpty) {
            textViewHint.visibility = View.VISIBLE
            return
        }

        if (hasFocus && isEmpty) {
            editText.hint = ""
            textViewHint.visibility = View.VISIBLE
            return
        }


    }

    private fun setTextChangedListener(editText: EditText, textView: TextView) {
        /** every time editText input changes calls */

        editText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkEditTextInput(editText, textView, s)
            }
        })
    }

    private fun setOnFocusChangeListener(editText: EditText, textViewHint: TextView, hint: String) {
        /** calls focusChange fun and notifies every time it changes **/

        editText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            val isEmpty: Boolean = editText.text!!.isEmpty()

            addEditTextFocusEffect(
                isEmpty,
                hasFocus,
                editText,
                textViewHint,
                hint
            )
        }
    }

    private fun checkEditTextInput(editText: EditText, textView: TextView, s: CharSequence) {
        /**checks  which editText is selected and verify the input to be ok for sending to server
         * and making button update it alpha to be disabled or not
         */

        //it is for focus change
//        if (editText.text!!.isEmpty())
//            editText.hint = ""

        if (type == FragmentType.SIGNUP_FRAGMENT)
            if (editText == mEditTextName) {
                if (editText.text!!.trim().length > 1) {
                    isName = true
                    addHintToTextView(isName, textView)
                } else {
                    isName = false
                    addHintToTextView(isName, textView)
                }
                checkButton()
                return
            }

        if (type == FragmentType.SIGNUP_FRAGMENT)
            if (editText == mEditTextFamilyName) {
                if (editText.text!!.trim().length > 1) {
                    isFamilyName = true
                    addHintToTextView(isFamilyName, textView)
                } else {
                    isFamilyName = false
                    addHintToTextView(isFamilyName, textView)
                }
                checkButton()
                return
            }

        if (editText == mEditTextPhoneNumber) {
            isNumber = isEmail(s)
            addHintToTextView(isNumber, textView)
            checkButton()
            return
        }

        if (editText == mEditTextPassword) {
            if (editText.text!!.trim().length > 7) {
                isPassword = true
                addHintToTextView(isPassword, textView)
            } else {
                isPassword = false
                addHintToTextView(isPassword, textView)
            }
            checkButton()
        }
    }

    private fun checkButton() {
        /** checks that every input be Ok and make button enable */

        if (isNumber && isFamilyName && isPassword && isName)
            mButtonComit?.apply {
                isClickable = true
                alpha = 1.0f
                isEnabled = true
            }
        else
            mButtonComit?.apply {
                isClickable = false
                alpha = 0.4f
                isEnabled = false
            }

    }

    private fun isEmail(s: CharSequence): Boolean {
        var expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        var pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        var matcher = pattern.matcher(s);
        return matcher.matches()
    }

    private fun addHintToTextView(isOk: Boolean, hintTextView: TextView) {
        /** Adds red or green COLOR to hint when client puts value in editText **/

        if (isOk) hintTextView.setTextColor(Color.parseColor("#1FBC8F"))
        else hintTextView.setTextColor(Color.parseColor("#FB3449"))
    }
}