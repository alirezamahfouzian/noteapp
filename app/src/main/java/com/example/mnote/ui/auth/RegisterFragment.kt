package com.example.mnote.ui.auth

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.e.fiting.ui.start_activity.ui.helpers.FragmentType
import com.example.mnote.R
import com.example.mnote.di.BaseApplication
import com.example.mnote.models.entry.RegisterEntry
import com.example.mnote.network.api.AuthApi
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.network.helper.Resource
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.AddEditTextFocusEffect
import com.example.mnote.ui.helper.FragmentKeys
import com.example.mnote.ui.helper.FragmentListener
import com.example.mnote.util.ErrorHandler.Companion.NO_INTERNET
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_register.*
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment(R.layout.fragment_register),
    View.OnClickListener {

    private val TAG = "LoginRegister"
    private var mSavedInstance: Bundle? = null
    private lateinit var navController: NavController
    private lateinit var mFragmentListener: FragmentListener
    private lateinit var mActivity: MainActivity
    private var mIsLogin = false
    private var token: String? = null
    lateinit var baseApplication: BaseApplication


    private val authViewModel: AuthViewModel by viewModels()

    @Inject
    lateinit var tokenInterceptor: TokenInterceptor

    @Inject
    lateinit var mPref: SharedPreferences

    @Inject
    lateinit var mAuthApi: AuthApi

    @Inject
    lateinit var mEditor: SharedPreferences.Editor

    @Inject
    lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as MainActivity
        mFragmentListener = mActivity
        mSavedInstance = mActivity.mInstanceState
        baseApplication = mActivity.application as BaseApplication

    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        build()
    }

    private fun build() {
        restoreFragment()
        textViewGoToLogin!!.setOnClickListener(this)
        imageViewBackLogin!!.setOnClickListener(this)
        buttonSubmitSignUp!!.setOnClickListener(this)
        addFocusEffect()
        register()
        getUser()
    }

    override fun onStop() {
        super.onStop()
        if (!mIsLogin)
            saveInstance()
    }

    private fun register() {
        authViewModel.registerLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Loading -> {
                    isLoading(true)
                }
                is Resource.Success -> {
                    token = it.data!!.token
                    token?.let {
                        mActivity.showProgressBar(false)
                        tokenInterceptor.token = token
                        authViewModel.getUser()
                    }
                }
                is Resource.Error -> {
                    isLoading(false)
                    mActivity.showSnackBar(it.message.toString())
                }
            }
        })
    }

    /** gets tiger when
     * {authViewModel.getUser()} get called*/
    private fun getUser() {
        authViewModel.userLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    mActivity.mInstanceState = null
                    mIsLogin = true
                    navController.popBackStack(R.id.loginFragment, true)
                    navController.navigate(R.id.homeFragment)
                }
                is Resource.Error -> {
                    mActivity.showSnackBar(it.message.toString())
                    isLoading(false)
                }
            }
        })
    }

    private fun isLoading(isLoading: Boolean) {
        if (isLoading) {
            buttonSubmitSignUp.isClickable = true
            constraintLayoutRegister.visibility = View.GONE
            mActivity.showProgressBar(isLoading)
        } else {
            buttonSubmitSignUp.isClickable = false
            constraintLayoutRegister.visibility = View.VISIBLE
            mActivity.showProgressBar(isLoading)
        }
    }

    private fun addFocusEffect() {
        AddEditTextFocusEffect(
            editTextName, textViewNameHint, editTextFamilyName,
            textViewFamilyNameHint, editTextEmail, textViewPhoneNumberHint,
            editTextPassword, textViewPasswordHint!!, buttonSubmitSignUp
        )
            .execute(FragmentType.SIGNUP_FRAGMENT)
    }

    private fun saveInstance() {
        mSavedInstance = Bundle()
        mSavedInstance?.putString(FragmentKeys.NAME_KEY, editTextName!!.text.toString())
        mSavedInstance?.putString(
            FragmentKeys.FAMILY_NAME_KEY,
            editTextFamilyName!!.text.toString()
        )
        mSavedInstance?.putString(FragmentKeys.EMAIL_KEY, editTextEmail!!.text.toString())
        mSavedInstance?.putString(FragmentKeys.PASSWORD_KEY, editTextPassword!!.text.toString())
        mFragmentListener.onBackPressedInstance(mSavedInstance)
    }

    private fun restoreFragment() {
        if (mSavedInstance != null) {
            mSavedInstance?.getString(FragmentKeys.NAME_KEY)?.let {
                editTextName!!.setText(it)
            }
            mSavedInstance?.getString(FragmentKeys.FAMILY_NAME_KEY)?.let {
                editTextFamilyName!!.setText(it)
            }
            mSavedInstance?.getString(FragmentKeys.EMAIL_KEY)?.let {
                editTextEmail!!.setText(it)
            }
            mSavedInstance?.getString(FragmentKeys.PASSWORD_KEY)?.let {
                editTextPassword!!.setText(it)
            }
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.textViewGoToLogin -> {
                navController.popBackStack()
                return
            }
            R.id.imageViewBackLogin -> {
                navController.popBackStack()
                return
            }
            R.id.buttonSubmitSignUp -> {
                if (connectionLiveData.isConnected) {
                    buttonSubmitSignUp.isClickable = false
                    var registerEntity = RegisterEntry(
                        editTextName.text.toString() + " " + editTextFamilyName.text.toString(),
                        editTextEmail.text.toString(),
                        editTextPassword.text.toString()
                    )
                    authViewModel.register(registerEntity)
                } else {
                    buttonSubmitSignUp.isClickable = true
                    mActivity.showSnackBar(NO_INTERNET)
                }
            }
        }
    }
}