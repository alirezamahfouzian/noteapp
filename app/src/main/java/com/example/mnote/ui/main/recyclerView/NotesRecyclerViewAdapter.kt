package com.example.mnote.ui.main.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mnote.R
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.main.home.HomeFragment
import com.example.mnote.ui.main.note.NoteClickListener
import javax.inject.Inject


class NotesRecyclerViewAdapter @Inject constructor(
    private val mContext: MainActivity,
    fragment: HomeFragment
) : RecyclerView.Adapter<NotesRecyclerViewAdapter.ViewHolder>() {
    val TAG = "NotesRecyclerViewAdapte"
    var mResponseList: List<NoteEntity> = ArrayList()
    private val noteClickListener: NoteClickListener

    fun setData(responseList: List<NoteEntity>) {
        val oldList = mResponseList

        val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(
            DataDiffUtilCallBack(oldList, responseList)
        )
        mResponseList = responseList
        diffResult.dispatchUpdatesTo(this)
    }

    init {
        noteClickListener = fragment
    }


    class DataDiffUtilCallBack(
        private val oldList: List<NoteEntity>,
        private val newList: List<NoteEntity>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].noteId == newList[newItemPosition].noteId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].equals(newList[newItemPosition].noteId)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext)
                .inflate(R.layout.row_main_grid, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val listNote = mResponseList[position]
        holder.mTextViewTitle!!.text = listNote.title
        holder.mTextViewNote!!.text = listNote.note
        holder.itemView.setOnClickListener {
            noteClickListener.onNoteClickListener(
                listNote,
                position
            )
        }
    }

    override fun getItemCount(): Int {
        return if (mResponseList.isNullOrEmpty()) 0 else mResponseList.size
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mTextViewTitle: TextView? = null
        var mTextViewNote: TextView? = null

        private fun init() {
            mTextViewTitle = itemView.findViewById(R.id.textViewTitle)
            mTextViewNote = itemView.findViewById(R.id.editTextNote)
        }

        init {
            init()
        }
    }
}

