package com.example.mnote.ui.main.note

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.mnote.R
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.ActivityHelper
import com.example.mnote.ui.helper.FragmentKeys.Companion.MODIFIED_TIME
import com.example.mnote.ui.helper.FragmentKeys.Companion.NOTE
import com.example.mnote.ui.helper.FragmentKeys.Companion.NOTE_ID
import com.example.mnote.ui.helper.FragmentKeys.Companion.TITLE
import com.example.mnote.ui.main.NoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_note.*
import javax.inject.Inject

@AndroidEntryPoint
class NoteFragment : Fragment(R.layout.fragment_note),
    View.OnClickListener {

    private var isDeleted: Boolean = false
    private var mNoteId: Int = -1
    private var mStartingTitle: String? = ""
    private var mStartingNote: String? = ""
    private var mTitle: String? = ""
    private var mNote: String? = ""
    private var mModifiedTime: String? = null
    private lateinit var mActivity: MainActivity

    @Inject
    lateinit var mHelper: ActivityHelper.Helper

    private val noteViewModel: NoteViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            getNote()
        }
        mActivity = activity as MainActivity
    }

    private fun getNote() {
        val bundle = requireArguments()
        mNoteId = bundle.getInt(NOTE_ID)
        mTitle = bundle.getString(TITLE)
        mStartingTitle = mTitle
        mNote = bundle.getString(NOTE)
        mStartingNote = mNote
        mModifiedTime = bundle.getString(MODIFIED_TIME)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        build()
    }

    override fun onStop() {
        super.onStop()
        if (!isDeleted)
            updateChanges()
        editTextTitle.setText("")
        editTextNote.setText("")
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.imageViewBack -> mActivity.onBackPressed()
            R.id.imageViewDelete -> {
                deleteNote()
                isDeleted = true
                mActivity.onBackPressed()
            }
        }
    }

    private fun build() {
        cast()
        // new note pass note_id = -1
        if (mNoteId != -1) {
            imageViewDelete.visibility = View.VISIBLE
            updateViewsData()
        } else {
            imageViewDelete.visibility = View.GONE
        }
    }

    private fun cast() {
        imageViewBack.setOnClickListener(this)
        imageViewDelete.setOnClickListener(this)
    }

    private fun updateChanges() {
        if (isDataChange() && !isNoteEmpty()) {
            if (mNoteId == -1)
                saveNote()
            else
                updateNote()
        }
    }

    private fun isDataChange(): Boolean {
        return !(mStartingTitle.equals(editTextTitle.text.toString()) &&
                mStartingNote.equals(editTextNote.text.toString()))
    }

    private fun isNoteEmpty(): Boolean {
        return (editTextTitle.text.toString().isBlank() && editTextNote.text.toString().isBlank())
    }

    private fun saveNote() {
        noteViewModel.addNote(
            NoteEntity(
                null,
                title = editTextTitle.text.toString(),
                note = editTextNote.text.toString(),
                modifiedTime = mHelper.getNowTime()
            )
        )
    }

    private fun updateNote() {
        noteViewModel.editNote(
            NoteEntity(
                mNoteId,
                title = editTextTitle.text.toString(),
                note = editTextNote.text.toString(),
                modifiedTime = mHelper.getNowTime()
            )
        )
    }

    private fun deleteNote() {
        noteViewModel.deleteNote(
            NoteEntity(
                mNoteId,
                null,
                null,
                null
            )
        )
    }

    private fun updateViewsData() {
        editTextTitle.setText(mTitle)
        editTextNote.setText(mNote)
    }
}
