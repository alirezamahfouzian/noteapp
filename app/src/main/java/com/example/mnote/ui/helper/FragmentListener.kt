package com.example.mnote.ui.helper

import android.os.Bundle

interface FragmentListener {
    fun onBackPressedInstance(saveInstance: Bundle?)

    companion object {
        const val NAME_KEY = "NAME_KEY"
        const val FAMILY_NAME_KEY = "FAMILY_NAME_KEY"
        const val NUMBER_KEY = "NUMBER_KEY"
        const val PASSWORD_KEY = "PASSWORD_KEY"
    }
}