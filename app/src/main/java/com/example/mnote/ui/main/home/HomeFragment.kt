package com.example.mnote.ui.main.home

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.mnote.R
import com.example.mnote.di.BaseApplication
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.network.helper.Resource
import com.example.mnote.persistence.AppDataBase
import com.example.mnote.session.SessionManger
import com.example.mnote.ui.MainActivity
import com.example.mnote.ui.helper.ActivityHelper
import com.example.mnote.ui.helper.FragmentKeys.Companion.GRID_KEY
import com.example.mnote.ui.helper.FragmentKeys.Companion.MODIFIED_TIME
import com.example.mnote.ui.helper.FragmentKeys.Companion.NOTE
import com.example.mnote.ui.helper.FragmentKeys.Companion.NOTE_ID
import com.example.mnote.ui.helper.FragmentKeys.Companion.THEME_PREFS_KEY
import com.example.mnote.ui.helper.FragmentKeys.Companion.TITLE
import com.example.mnote.ui.main.NoteViewModel
import com.example.mnote.ui.main.note.NoteClickListener
import com.example.mnote.ui.main.note.NoteFragment
import com.example.mnote.ui.main.recyclerView.NotesRecyclerViewAdapter
import com.example.mnote.ui.main.search.SearchFragment
import com.example.mnote.util.ErrorHandler.Companion.NO_INTERNET
import com.google.android.material.appbar.AppBarLayout
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@SuppressLint("CheckResult")
@AndroidEntryPoint
class HomeFragment : Fragment(),
    View.OnClickListener, NoteClickListener {

    private val TAG = "HomeFragment"

    @Inject
    lateinit var helper: ActivityHelper.Helper

    @Inject
    lateinit var mPref: SharedPreferences

    @Inject
    lateinit var mEditor: SharedPreferences.Editor

    @Inject
    lateinit var bundle: Bundle

    @Inject
    lateinit var tokenInterceptor: TokenInterceptor

    @Inject
    lateinit var sessionManger: SessionManger

    @Inject
    lateinit var appDataBase: AppDataBase

    @Inject
    lateinit var connectionLiveData: ConnectionLiveData

    private lateinit var mActivity: MainActivity

    private var navController: NavController? = null

    private val noteViewModel: NoteViewModel by activityViewModels()

    private lateinit var scrollListener: RecyclerView.OnScrollListener

    private var isLight = true
    private var mIsGrid: Boolean = true
    private lateinit var mDrawableGrid: Drawable
    private lateinit var mDrawableList: Drawable
    private lateinit var mDrawableProfile: Drawable
    private lateinit var mStaggerdLayoutManager: StaggeredGridLayoutManager
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mAdapter: NotesRecyclerViewAdapter
    private var savedViewInstance: View? = null
    lateinit var baseApplication: BaseApplication


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as MainActivity
        baseApplication = mActivity.application as BaseApplication
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: ")
        return if (savedViewInstance != null) {
            savedViewInstance
        } else {
            savedViewInstance =
                inflater.inflate(R.layout.fragment_home, container, false)
            savedViewInstance
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated: ")
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        build()
    }

    override fun onClick(view: View) {
        fragmentNavHost
        when (view.id) {
            R.id.textViewSearch -> helper.transitToFragment(mActivity, SearchFragment(), true)
            R.id.imageViewView -> changeView()
            R.id.floatingActionButton -> {
                bundle.putInt(NOTE_ID, -1)
                helper.transitToFragment(
                    mActivity,
                    NoteFragment().apply { arguments = bundle },
                    true
                )
            }
        }
    }

    override fun onNoteClickListener(note: NoteEntity, position: Int) {
        val bundle = Bundle()
        bundle.putInt(NOTE_ID, note.noteId!!)
        bundle.putString(NOTE, note.note)
        bundle.putString(TITLE, note.title)
        bundle.putString(MODIFIED_TIME, note.modifiedTime)
        helper.transitToFragment(mActivity, NoteFragment().apply { arguments = bundle }, true)
    }

    private fun build() {
        cast()
        changeThemeMenu()
        setRecyclerViewNote()
    }

    private fun cast() {
        initDrawables()
        changeViewAtStart()
        changeProfileFrame(isLight)
        floatingActionButton!!.setOnClickListener(this)
        imageViewView!!.setOnClickListener(this)
        textViewSearch!!.setOnClickListener(this)
        swipeRefresh.setOnRefreshListener {
            if (connectionLiveData.isConnected) {
                noteViewModel.getNotesCache()
            } else {
                swipeRefresh.isRefreshing = false
                mActivity.showSnackBar(NO_INTERNET)
            }
        }
        // fixing swipeRefresh scrolling bug
        setRecyclerViewScrollListener()

    }

    private fun initDrawables() {
        mDrawableGrid = ContextCompat
            .getDrawable(mActivity, R.drawable.ic_grid)!!
        mDrawableList = ContextCompat
            .getDrawable(mActivity, R.drawable.ic_list)!!
        mDrawableProfile = ContextCompat
            .getDrawable(mActivity, R.drawable.ic_frame_profile)!!
    }

    private fun changeThemeMenu() {
        imageViewTheme!!.setOnClickListener {
            val popup = PopupMenu(mActivity, imageViewTheme!!)
            popup.menuInflater.inflate(R.menu.main_theme_menu, popup.menu)
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menuTheme -> {
                        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                            changeTheme(true)
                        } else {
                            changeTheme(false)
                        }
                    }
                    R.id.menuLogout -> {
                        lifecycleScope.launch(IO) {
                            appDataBase.clearAllTables()
                            withContext(Main) {
                                navController!!.navigate(R.id.action_homeFragment_to_loginFragment)
                            }
                        }
                    }
                }
                true
            }
            popup.show()
        }
    }

    private fun changeTheme(isLight: Boolean) {
        mEditor.putBoolean(THEME_PREFS_KEY, isLight).apply()
        changeProfileFrame(isLight)
        if (isLight)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }

    private fun changeView() {
        mIsGrid = mPref.getBoolean(GRID_KEY, true)
        if (mIsGrid) {
            imageViewView!!.setImageDrawable(mDrawableList)
            mEditor.putBoolean(GRID_KEY, false).apply()
        } else {
            imageViewView!!.setImageDrawable(mDrawableGrid)
            mEditor.putBoolean(GRID_KEY, true).apply()
        }
        refreshRecyclerView()
    }

    private fun setRecyclerViewScrollListener() {
        var isAppBar = false
        appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            Log.d("fixAppBarBug", "cast: $verticalOffset ")
            isAppBar = verticalOffset == 0
        })
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(-1) &&
                    newState == RecyclerView.SCROLL_STATE_IDLE &&
                    isAppBar
                ) {
                    Log.d("fixAppBarBug", "onScrollStateChanged:")
                    swipeRefresh.isEnabled = true
                } else {
                    Log.d("fixAppBarBug", "onScrollStateChanged: elseelse")
                    if (swipeRefresh.isEnabled)
                        swipeRefresh.isEnabled = false
                }
            }
        }
        recyclerViewNote.addOnScrollListener(scrollListener)
    }

    private fun changeViewAtStart() {
        mIsGrid = mPref.getBoolean(GRID_KEY, true)
        if (mIsGrid) {
            imageViewView!!.setImageDrawable(mDrawableGrid)
        } else {
            imageViewView!!.setImageDrawable(mDrawableList)
        }
    }

    private fun changeProfileFrame(isLight: Boolean) {
        if (isLight) {
            imageViewProfile!!.setImageDrawable(mDrawableProfile)
        } else {
            imageViewProfile!!.setImageDrawable(mDrawableProfile)
        }
    }

    private fun setRecyclerViewNote() {
        mAdapter = NotesRecyclerViewAdapter(mActivity, this)
        mStaggerdLayoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        mLinearLayoutManager = LinearLayoutManager(mActivity)
        recyclerViewNote!!.setHasFixedSize(true)
        recyclerViewNote!!.adapter = mAdapter
        if (mIsGrid) {
            recyclerViewNote!!.layoutManager = mStaggerdLayoutManager
        } else {
            recyclerViewNote!!.layoutManager = mLinearLayoutManager
        }
        setRecyclerViewData()
    }

    private fun setRecyclerViewData() {
        noteViewModel.getNotesCache()

        noteViewModel.allNotes.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Loading -> {
                    if (it.data.isNullOrEmpty()) {
                        if (!swipeRefresh.isRefreshing)
                            showProgressBar(true)
                        Log.d("testtest", "setRecyclerViewData: loading not have")
                    } else {
                        Log.d("testtest", "setRecyclerViewData: loading have")
                        haveNote(true)
                        setData(it.data)
                    }
                }
                is Resource.Success -> {
                    if (it.data.isNullOrEmpty()) {
                        Log.d("testtest", "setRecyclerViewData: Success not have")
                        haveNote(false)
                    } else {
                        haveNote(true)
                        Log.d("testtest", "setRecyclerViewData: Success have")
                    }
                    setData(it.data!!)

                }
                is Resource.Error -> {
                    if (it.data.isNullOrEmpty()) {
                        Log.d("testtest", "setRecyclerViewData: Error not have")

                        haveNote(false)
                    } else {
                        Log.d("testtest", "setRecyclerViewData: Error have")
                        haveNote(true)
                    }
                    setData(it.data!!)
                }
            }
        })
    }

    private fun haveNote(haveNote: Boolean) {
        if (haveNote) {
            recyclerViewNote.visibility = View.VISIBLE
            constrainLayoutNoNote.visibility = View.GONE
        } else {
            recyclerViewNote.visibility = View.GONE
            constrainLayoutNoNote.visibility = View.VISIBLE
        }
    }

    private fun showProgressBar(isVisible: Boolean) {
        mActivity.showProgressBar(isVisible)
    }

    private fun setData(note: List<NoteEntity>) {
        val oldListSize = mAdapter.mResponseList.size
        val newListSize = note.size
        mAdapter.setData(note)
        // checks if the operation was delete or edit and insert
        if (oldListSize <= newListSize) {
            Log.d(TAG, "setData: $oldListSize  $newListSize ")
            recyclerViewNote.smoothScrollToPosition(0)
        }
        showProgressBar(false)
        swipeRefresh.isRefreshing = false
    }

    private fun refreshRecyclerView() {
        if (!mIsGrid) {
            recyclerViewNote!!.layoutManager = mStaggerdLayoutManager
        } else {
            recyclerViewNote!!.layoutManager = mLinearLayoutManager
        }
    }

}