package com.example.mnote.ui.main.note

import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.models.response.NoteModel
import com.example.mnote.models.response.NoteResponse

interface NoteClickListener {
    fun onNoteClickListener(note: NoteEntity, position: Int)

}