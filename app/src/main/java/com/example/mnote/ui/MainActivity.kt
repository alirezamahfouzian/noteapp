package com.example.mnote.ui

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.mnote.R
import com.example.mnote.di.BaseApplication
import com.example.mnote.network.api.TokenInterceptor
import com.example.mnote.network.helper.ConnectionLiveData
import com.example.mnote.ui.auth.AuthViewModel
import com.example.mnote.ui.helper.ActivityHelper
import com.example.mnote.ui.helper.FragmentListener
import com.example.mnote.ui.main.NoteViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), FragmentListener {

    var mInstanceState: Bundle? = null
    lateinit var snackBar: Snackbar
    private var mSnackbarFont: Typeface? = null

    lateinit var baseApplication: BaseApplication

    @Inject
    lateinit var tokenInterceptor: TokenInterceptor

    @Inject
    lateinit var helper: ActivityHelper.Helper

    @Inject
    lateinit var connectionLiveData: ConnectionLiveData

    private var navController: NavController? = null

    val authViewModel: AuthViewModel by viewModels()

    private val noteViewModel: NoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        helper.setRotationPortrait(this)
        baseApplication = application as BaseApplication
        checkAuth()
        setContentView(R.layout.activity_main)
        build()
    }

    private fun build() {
        onConnected()
        setToken()
        init()
    }

    private fun onConnected() {
        connectionLiveData.observeForever {
            Log.d(MainActivity.TAG, "checkNetwork: ${it?.isConnected}")
            if(it!!.isConnected){}
//                noteViewModel.syncServerOperations()
        }
    }

    /**
     *  check database for token existence
     * if exists user is authenticated else
     * go to login fragment for authentication
     */
    private fun checkAuth() {
        authViewModel.getToken().observe(this, Observer { token ->
            token?.let {
                token.token?.let {
                    authViewModel.userId = token.userId
                    handleNavigation(true)
                }
            } ?: handleNavigation(false)
        })
    }

    /**
     * changes navigation starting fragment based on
     * user auth
     */
    private fun handleNavigation(isLoggedIn: Boolean) {
        navController = Navigation.findNavController(this, R.id.fragmentNavHost)
        val navGraph = navController!!.navInflater.inflate(R.navigation.nav_graph)
        if (isLoggedIn) {
            navGraph.startDestination = R.id.homeFragment
        } else {
            navGraph.startDestination = R.id.loginFragment
        }
        navController?.graph = navGraph
    }

    /** set the token to TokenInterceptor
     * on start of activity from db*/
    private fun setToken() {
        authViewModel.getToken().observe(this, Observer {
            it?.let {
                tokenInterceptor.token = it.token
            }
        })
    }

    private fun init() {
        createSnack()
        mSnackbarFont = ResourcesCompat.getFont(this, R.font.iransans_medium)
    }

    override fun onBackPressed() {
        if (navController?.currentDestination?.label == "RegisterFragment") {
            navController?.popBackStack()
        } else {
            super.onBackPressed()
        }
        hideKeyboard(this)
    }

    override fun onBackPressedInstance(saveInstance: Bundle?) {
        mInstanceState = saveInstance!!
    }

    private fun createSnack() {
        snackBar =
            Snackbar.make(findViewById(android.R.id.content), "", 4000).apply {
                setBackgroundTint(resources.getColor(R.color.red_text))
                setActionTextColor(resources.getColor(R.color.white))
                setTextColor(resources.getColor(R.color.white))
            }
        val snackView: View = snackBar.view
        ViewCompat.setLayoutDirection(snackView, ViewCompat.LAYOUT_DIRECTION_LTR)
        snackView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            .apply {
                textAlignment = View.TEXT_ALIGNMENT_CENTER
                typeface = mSnackbarFont
                setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.get_info_snackbar_text_size)
                )
            }
        snackView.findViewById<TextView>(com.google.android.material.R.id.snackbar_action)
            .apply {
                setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.get_info_snackbar_text_size)
                )
                typeface = mSnackbarFont
            }
    }

    fun showSnackBar(msg: String) {
        snackBar.setText(msg).show()
    }

    fun showProgressBar(isVisible: Boolean) {
        if (isVisible)
            progressBar.visibility = View.VISIBLE
        else
            progressBar.visibility = View.GONE
    }

    companion object {
        const val TAG = "MainActivitytt"
        fun hideKeyboard(context: Context) {
            try {
                (context as Activity).window
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                if (context.currentFocus != null && context.currentFocus!!
                        .windowToken != null
                ) {
                    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        context.currentFocus!!.windowToken,
                        0
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}