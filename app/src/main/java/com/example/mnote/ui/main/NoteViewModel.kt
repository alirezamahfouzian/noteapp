package com.example.mnote.ui.main

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.network.helper.Resource
import com.example.mnote.repository.main.NoteRepository

class NoteViewModel @ViewModelInject constructor(
    val noteRepository: NoteRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel(), LifecycleObserver {

    private val TAG = "NoteViewModel"

    private val _allNotes: MediatorLiveData<Resource<List<NoteEntity>>> = MediatorLiveData()
    val allNotes: LiveData<Resource<List<NoteEntity>>> get() = _allNotes

    fun syncServerOperations() {
        noteRepository.syncServerOperations()
    }

    fun getNotesCache() {
        val notes = noteRepository.getNotes()
        _allNotes.addSource(notes, Observer {
            _allNotes.value = it
        })
    }

    fun addNote(noteEntity: NoteEntity) {
        return noteRepository.addNote(noteEntity)
    }

    fun editNote(noteEntity: NoteEntity) {
        return noteRepository.editNote(noteEntity)
    }

    fun deleteNote(noteEntity: NoteEntity) {
        return noteRepository.deleteNote(noteEntity)
    }

}