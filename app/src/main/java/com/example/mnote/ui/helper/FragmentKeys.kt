package com.example.mnote.ui.helper

class FragmentKeys {
    companion object {

        // login
        const val ARG_PARAM1 = "param1"
        const val IS_AUTH = "IS_AUTH"
        const val SHARED_PREF = "SHARED_PREF"
        val TAG = "debugLog"
        val NUMBER_KEY = "NUMBER_KEY"
        val PASSWORD_KEY = "PASSWORD_KEY"
        val TOKEN_KEY = "TOKENTOKEN_KEY_KEY"

        // register
        var NAME_KEY = "NAME_KEY"
        var FAMILY_NAME_KEY = "FAMILY_NAME_KEY"
        var EMAIL_KEY = "NUMBER_KEY"

        // home fragment
        val NOTE_ID = "noteId"
        val USER_ID = "userId"
        val NOTE = "note"
        val TITLE = "title"
        val MODIFIED_TIME = "modifiedTime"
        val THEME_PREFS_KEY = "THEME_PREFS_KEY"
        val GRID_KEY = "GRID_KEY"
    }
}
