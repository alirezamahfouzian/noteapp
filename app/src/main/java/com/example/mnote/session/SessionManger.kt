package com.example.mnote.session

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mnote.models.entity.TokenEntity
import com.example.mnote.persistence.TokenDao
import com.example.mnote.ui.helper.FragmentKeys.Companion.TAG
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.CancellationException
import javax.inject.Inject

class SessionManger @Inject constructor(
    private val tokenDao: TokenDao
) {

    private val _cachedToken = MutableLiveData<TokenEntity?>()

    private val cachedToken: LiveData<TokenEntity?> get() = _cachedToken

    /** add the token to live data so that observes know that user is valid */
    fun login(newToken: TokenEntity) {
        setValue(newToken)
    }

    /** remove the token from database and notify observers*/
    fun logout() {
        GlobalScope.launch(IO) {
            var errorMessage: String? = null
            try {
                cachedToken.value!!.userId?.let {
                    tokenDao.deleteToken(it)
                }
            } catch (e: CancellationException) {
                Log.e(TAG, "logout: ", e)
                errorMessage = e.message
            } catch (e: Exception) {
                errorMessage = e.message + "\n" + e.message
                Log.e(TAG, "logout: ", e)
            } finally {
                errorMessage?.let {
                    Log.e(TAG, "logout: $errorMessage")
                }
                Log.d(TAG, "logout: finally")
                // remove the token from liveData
                setValue(null)
            }
        }
    }

    private fun setValue(newValue: TokenEntity?) {
        GlobalScope.launch(Main) {
            if (_cachedToken.value != newValue)
                _cachedToken.value = newValue
        }
    }

}