package com.example.mnote.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mnote.models.entity.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUserReplace(userEntity: UserEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addUserIgnore(userEntity: UserEntity): Long

    @Query("SELECT * FROM users WHERE user_id= :userId")
    fun searchById(userId: Int): LiveData<UserEntity>

}