package com.example.mnote.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.mnote.models.entity.TokenEntity

@Dao
interface TokenDao {

    @Insert
    suspend fun addToken(tokenEntity: TokenEntity): Long

    @Query("DELETE FROM token WHERE user_id= :userId")
    suspend fun deleteToken(userId: Int): Int

    @Query("SELECT * FROM token")
    fun getToken(): LiveData<TokenEntity>
}