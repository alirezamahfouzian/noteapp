package com.example.mnote.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.example.mnote.models.entity.NoteEntity

@Dao
interface NoteDao {

    @Query("SELECT * FROM note ORDER BY note.modified_time DESC")
    fun getAllNotes(): LiveData<List<NoteEntity>>
//    SELECT * FROM `notes` ORDER BY `notes`.`created_time` DESC

    @Query("SELECT * FROM note where note_id = :noteId ")
    fun getNote(noteId: Int): LiveData<NoteEntity>

    @Insert(onConflict = REPLACE)
    fun addNote(noteEntity: NoteEntity): Long

    @Query("UPDATE note SET note_id = :newNoteId where note_id = :oldNoteId")
    fun updateNoteId(oldNoteId: Int, newNoteId: Int): Int

    @Update(onConflict = REPLACE)
    fun updateNote(noteEntity: NoteEntity): Int

    @Delete()
    fun deleteNote(noteEntity: NoteEntity): Int

}