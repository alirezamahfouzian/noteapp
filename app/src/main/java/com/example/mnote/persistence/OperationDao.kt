package com.example.mnote.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.mnote.models.entity.OperationEntity

@Dao
interface OperationDao {

    @Query("SELECT * FROM operation")
    fun getOperations(): List<OperationEntity>

    @Insert(onConflict = REPLACE)
    fun addOperation(operationEntity: OperationEntity): Long

    @Delete
    fun deleteOperation(operationEntity: OperationEntity): Int
}