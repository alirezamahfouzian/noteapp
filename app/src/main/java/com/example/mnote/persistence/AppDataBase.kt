package com.example.mnote.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.models.entity.OperationEntity
import com.example.mnote.models.entity.TokenEntity
import com.example.mnote.models.entity.UserEntity

@Database(
    entities = [UserEntity::class, NoteEntity::class, TokenEntity::class, OperationEntity::class],
    version = 1
)
abstract class AppDataBase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getNoteDao(): NoteDao

    abstract fun getTokenDao(): TokenDao

    abstract fun getOperationDao(): OperationDao

    companion object {
        const val DATABASE_NAME = "note_db"
    }

}