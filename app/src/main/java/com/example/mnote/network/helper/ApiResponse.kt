package com.example.mnote.network.helper

import retrofit2.Response
import java.io.IOException

open class ApiResponse<T> {
    fun create(error: Throwable): ApiResponse<T> {
        return ApiErrorResponse(if (error.message == "") error.message else "Unknown error\nCheck network connection")
    }

    fun create(response: Response<T>): ApiResponse<T> {
        return if (response.isSuccessful) {
            val body: T? = response.body()
            if (body == null || response.code() === 204) { // 204 is empty response
                ApiEmptyResponse<T>()
            } else {
                ApiSuccessResponse(body)
            }
        } else {
            var errorMsg = ""
            errorMsg = try {
                response.errorBody()?.toString()!!
            } catch (e: IOException) {
                e.printStackTrace()
                response.message()
            }
            ApiErrorResponse(errorMsg)
        }
    }

    /**
     * Generic success response from api
     * @param <T>
    </T> */
    class ApiSuccessResponse<T> internal constructor(val body: T) : ApiResponse<T>()

    /**
     * Generic Error response from API
     * @param <T>
    </T> */
    class ApiErrorResponse<T> internal constructor(val errorMessage: String?) :
        ApiResponse<T>()

    /**
     * separate class for HTTP 204 resposes so that we can make ApiSuccessResponse's body non-null.
     */
    class ApiEmptyResponse<T> : ApiResponse<T>()
}