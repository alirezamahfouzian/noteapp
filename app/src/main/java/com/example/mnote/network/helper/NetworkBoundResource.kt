package com.example.mnote.network.helper

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


// CashObject: Type for the Resource data.
// RequestObject: Type for the API response.
abstract class NetworkBoundResource<CashObject, RequestObject> {

    private val results: MediatorLiveData<Resource<CashObject>> = MediatorLiveData()
    private val disposable: CompositeDisposable = CompositeDisposable()

    constructor() {
        init()
    }

    private fun init() {
        // update live data for loading status
        results.value = Resource.Loading(null)

        // observe liveData from local db
        val dbSource: LiveData<CashObject> = loadFromDb()

        results.addSource(dbSource, Observer {
            results.removeSource(dbSource)

            if (shouldFetch(it)) {
                // get data from the network
            } else {
                results.addSource(dbSource, Observer {
                    setValue(Resource.Success(it))
                })
            }

        })
    }

    fun setValue(newValue: Resource<CashObject>) {
        if (results.value != newValue)
            results.value = newValue
    }

    fun fetchFromNetwork(dbSource: LiveData<CashObject>) {
        results.addSource(dbSource, Observer {
            setValue(Resource.Loading(it))
        })
        val apiResponse: LiveData<ApiResponse<RequestObject>> = createCall()
        results.addSource(apiResponse, Observer { itRes ->
            results.removeSource(dbSource)
            results.removeSource(apiResponse)

            if (itRes is ApiResponse.ApiSuccessResponse) {
                var observable: Observable<Void> = Observable.create { item ->
                    saveCallResult(null)
                    item.onComplete()
                }
                var dis = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        results.addSource(loadFromDb(), Observer {
                            setValue(Resource.Success(it))
                        })
                    }
                disposable.add(dis)


            } else if (itRes is ApiResponse.ApiEmptyResponse) {

            } else if (itRes is ApiResponse.ApiErrorResponse) {

            }


        })
    }

//    fun processResponse(response : ApiResponse.ApiSuccessResponse<*>) : CashObject{
//        return
//    }

    // Called to save the result of the API response into the database
    @WorkerThread
    protected abstract fun saveCallResult(item: RequestObject?)

    // Called with the data in the database to decide whether to fetch
    // potentially updated data from the network.
    @MainThread
    protected abstract fun shouldFetch(data: CashObject?): Boolean

    // Called to get the cached data from the database.
    @MainThread
    protected abstract fun loadFromDb(): LiveData<CashObject>

    // Called to create the API call.
    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestObject>>

    // Called when the fetch fails. The child class may want to reset components
    // like rate limiter.
    protected open fun onFetchFailed() {}

    // Returns a LiveData object that represents the resource that's implemented
    // in the base class.
    fun asLiveData(): LiveData<Resource<CashObject>> = results
}
