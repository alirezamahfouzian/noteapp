package com.example.mnote.network.api

import androidx.lifecycle.LiveData
import com.example.mnote.models.response.NoteChangeResponse
import com.example.mnote.models.response.NoteResponse
import com.example.mnote.util.ApiResponse
import retrofit2.http.*


interface NoteApi {

    @GET("/note/getNotes.php")
    fun getAllNotes(): LiveData<ApiResponse<NoteResponse>>

    @FormUrlEncoded
    @POST("/note/addNote.php")
    fun addNote(
        @Field("title") title: String,
        @Field("note") note: String,
        @Field("modified_time") modifiedTime: String
    ): LiveData<ApiResponse<NoteChangeResponse>>

    @FormUrlEncoded
    @POST("/note/editeNote.php")
    fun editNote(
        @Field("note_id") noteId: Int,
        @Field("title") title: String,
        @Field("note") note: String,
        @Field("modified_time") modifiedTime: String
    ): LiveData<ApiResponse<NoteChangeResponse>>

    @FormUrlEncoded
    @POST("/note/deleteNote.php")
    fun deleteNote(@Field("note_id") noteId: Int): LiveData<ApiResponse<NoteChangeResponse>>

}