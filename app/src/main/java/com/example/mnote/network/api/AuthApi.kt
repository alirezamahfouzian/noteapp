package com.example.mnote.network.api

import com.example.mnote.models.entry.LoginEntry
import com.example.mnote.models.entry.RegisterEntry
import com.example.mnote.models.response.LoginResponse
import com.example.mnote.models.response.RegisterResponse
import com.example.mnote.models.response.UserResponse
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST


interface AuthApi {

    @POST("php-login-registration-api/login.php")
    fun login(@Body loginEntry: LoginEntry): Call<LoginResponse>

    @POST("php-login-registration-api/register.php")
    fun register(@Body registerEntry: RegisterEntry): Call<RegisterResponse>

    @POST("php-login-registration-api/userInfo.php")
    fun getUser(): Call<UserResponse>
}