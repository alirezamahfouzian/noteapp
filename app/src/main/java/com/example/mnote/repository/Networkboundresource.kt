package com.example.mnote.repository

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.example.mnote.network.helper.Resource
import com.example.mnote.util.ApiEmptyResponse
import com.example.mnote.util.ApiErrorResponse
import com.example.mnote.util.ApiResponse
import com.example.mnote.util.ApiSuccessResponse
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

// CashObject: Type for the Resource data.
// RequestObject: Type for the API response.
abstract class NetworkBoundResource<CashObject, RequestObject> {

    private val TAG = "Networkboundresource"
    private val results: MediatorLiveData<Resource<CashObject>> = MediatorLiveData()
    private lateinit var job: CompletableJob
    private lateinit var coroutineScope: CoroutineScope

    init {
        initJob()
        results.value = Resource.Loading(null)

        // observe liveData from cash
        val dbSource: LiveData<CashObject> = loadFromDb()

        results.addSource(dbSource, Observer {
            results.removeSource(dbSource)

            // update live data for loading status
            setValue(Resource.Loading(it))

            if (shouldFetch(it)) {
                // get data from network
                val networkSync: LiveData<Boolean> = syncNetwork()

                results.addSource(networkSync, Observer { syncDone ->
                    results.removeSource(networkSync)
                    Log.d(TAG, "trans:$syncDone ")
                    if (syncDone) {
                        GlobalScope.launch(Main) {
                            delay(500)
                            fetchFromNetwork(dbSource)
                        }
                    }
                })
            } else {
                // get data from the cash
                results.addSource(dbSource, Observer { cash ->
                    results.value = Resource.Success(cash)
                })
            }
        })
    }

    private fun fetchFromNetwork(dbSource: LiveData<CashObject>) {
        Log.d(TAG, "fetchFromNetwork: Called")

        val apiResponse: LiveData<ApiResponse<RequestObject>> = createCall()

        results.addSource(apiResponse, Observer { requestObject ->
            results.removeSource(dbSource)
            results.removeSource(apiResponse)

            when (requestObject) {
                is ApiSuccessResponse -> {
                    Log.d(TAG, "fetchFromNetwork: success")
                    coroutineScope.launch {
                        saveCallResult(requestObject.body)
                        withContext(Main) {
                            results.addSource(loadFromDb(), Observer { cash ->
                                setValue(Resource.Success(cash))
                            })
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    Log.d(TAG, "fetchFromNetwork: empty")
                    results.addSource(loadFromDb(), Observer {
                        setValue(Resource.Success(it))
                    })
                }
                is ApiErrorResponse -> {
                    Log.d(TAG, "fetchFromNetwork: error")
                    results.addSource(dbSource, Observer {
                        setValue(Resource.Error(requestObject.errorMessage, it))
                    })
                }
            }

        })

    }

    @MainThread
    private fun setValue(newValue: Resource<CashObject>) {
        if (results.value != newValue) results.postValue(newValue)
    }

    private fun initJob(): Job {
        job = Job()
        coroutineScope = CoroutineScope(IO + job)
        return job
    }

    // Called to save the result of the API response into the database
    @WorkerThread
    protected abstract suspend fun saveCallResult(item: RequestObject?)

    // Called with the data in the database to decide whether to fetch
    // potentially updated data from the network.
    @MainThread
    protected abstract fun shouldFetch(data: CashObject?): Boolean

    // Called to get the cached data from the database.
    @MainThread
    protected abstract fun loadFromDb(): LiveData<CashObject>

    // Called to create the API call.
    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestObject>>

    protected abstract fun syncNetwork(): LiveData<Boolean>

    fun asLiveData() = results as LiveData<Resource<CashObject>>

}
