package com.example.mnote.repository.auth

import androidx.lifecycle.LiveData
import com.example.mnote.models.entry.LoginEntry
import com.example.mnote.models.entry.RegisterEntry
import com.example.mnote.models.entity.TokenEntity
import com.example.mnote.models.entity.UserEntity
import com.example.mnote.models.response.LoginResponse
import com.example.mnote.models.response.RegisterResponse
import com.example.mnote.models.response.UserResponse
import com.example.mnote.network.api.AuthApi
import com.example.mnote.persistence.TokenDao
import com.example.mnote.persistence.UserDao
import io.reactivex.Flowable
import retrofit2.Call
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val authApi: AuthApi,
    private val tokenDao: TokenDao,
    private val userDao: UserDao
) {

    fun login(loginEntry: LoginEntry): Call<LoginResponse> {
        return authApi.login(loginEntry)
    }

    fun register(registerEntry: RegisterEntry): Call<RegisterResponse> {
        return authApi
            .register(registerEntry)
    }

    fun getUser(): Call<UserResponse> {
        return authApi.getUser()
    }

    suspend fun addToken(tokenEntity: TokenEntity): Long {
        return tokenDao.addToken(tokenEntity)
    }

    fun getToken(): LiveData<TokenEntity> {
        return tokenDao.getToken()
    }

    suspend fun deleteToken(userId: Int): Int {
        return tokenDao.deleteToken(userId)
    }

    suspend fun addUserReplace(userEntity: UserEntity): Long {
        return userDao.addUserReplace(userEntity)
    }

}