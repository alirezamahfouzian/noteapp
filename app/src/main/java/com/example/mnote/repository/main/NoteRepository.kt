package com.example.mnote.repository.main

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.mnote.models.entity.NoteEntity
import com.example.mnote.models.entity.OperationEntity
import com.example.mnote.models.response.NoteChangeResponse
import com.example.mnote.models.response.NoteResponse
import com.example.mnote.network.api.NoteApi
import com.example.mnote.network.helper.Resource
import com.example.mnote.persistence.NoteDao
import com.example.mnote.persistence.OperationDao
import com.example.mnote.repository.NetworkBoundResource
import com.example.mnote.util.ApiResponse
import com.example.mnote.util.NoteOperationHandler
import javax.inject.Inject

class NoteRepository @Inject constructor(
    val noteApi: NoteApi,
    val noteDao: NoteDao,
    val noteOperationHandler: NoteOperationHandler
    ) {
    private val TAG = "HomeFragment"

    fun syncServerOperations(): LiveData<Boolean> {
        return noteOperationHandler.syncServerOperations()
    }

    fun getNotes(): LiveData<Resource<List<NoteEntity>>> {
        return object : NetworkBoundResource<List<NoteEntity>, NoteResponse>() {
            override suspend fun saveCallResult(item: NoteResponse?) {
                if (item?.success == 1) {
                    Log.d(TAG, "saveCallResult: ${item.notes}")
                    item.notes?.forEach {
                        noteDao.addNote(it)
                    }
                }
            }

            override fun shouldFetch(data: List<NoteEntity>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<NoteEntity>> {
                return noteDao.getAllNotes()
            }

            override fun createCall(): LiveData<ApiResponse<NoteResponse>> {
                return noteApi.getAllNotes()
            }

            override fun syncNetwork(): LiveData<Boolean> {
                return syncServerOperations()
            }

        }.asLiveData()
    }

    fun addNote(noteEntity: NoteEntity) {
        noteOperationHandler.addNote(noteEntity)
    }

    fun editNote(noteEntity: NoteEntity) {
        noteOperationHandler.editNote(noteEntity)
    }

    fun deleteNote(noteEntity: NoteEntity){
        noteOperationHandler.deleteNote(noteEntity)
    }

//    fun getOperations(): LiveData<List<OperationEntity>> {
//        return operationDao.getOperations()
//    }
//
//    fun addOperation(operationEntity: OperationEntity): Long {
//        return operationDao.addOperation(operationEntity)
//    }
//
//    fun deleteOperation(operationEntity: OperationEntity): Int {
//        return operationDao.deleteOperation(operationEntity)
//    }


}