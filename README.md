## Used technologies:

* Mvvm architecture
* Retrofit 2 and Gson	
* Dagger hilt
* Android Room
* Database Caching
* RxJava
* kotlin Coroutine
* Android navigation Component
* Leak canary
* LiveData
* Android material design
* Dark mode

## Description:

This program is a simple note app that uses Retrofit and Room persistent library to cach the notes and simultaneously sends it to the server and shows the offline notes from the Database to the user.

## point:
This program is still under development and is not fully finished and may have some bugs.

## Screenshot:

Screenshots are placed in 'Screenshot' folder
